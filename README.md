# SonarCloud analyzing example for ASP.NET Core and React SPA
[![pipeline status](https://gitlab.com/gmarokov/sonar-aspnet-react-example/badges/master/pipeline.svg)](https://gitlab.com/gmarokov/sonar-aspnet-react-example/commits/master)

This project is a showcase for a blog post which can found here: 
https://dev.to/gmarokov/analyze-asp-net-core-with-your-react-spa-in-sonarcloud-5goj

## Frontend
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=alert_status)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=bugs)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=code_smells)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=coverage)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=ncloc)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=security_rating)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.frontend&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sonar.example.frontend)

### Startup
`cd Frontend`  
`npm install`  
`npm start`

### Test
`cd Frontend`  
`npm run test`

## Backend
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=alert_status)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=bugs)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=code_smells)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=coverage)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=ncloc)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=security_rating)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sonar.example.backend)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sonar.example.backend&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sonar.example.backend)

### Startup
`cd .\Backend\Backend.Api\`  
`dotnet run`

### Test
`cd .\Backend`  
`dotnet test`

### Docker hub images
Tags available for 5.0 and 3.1:  
https://hub.docker.com/r/gmarokov/sonar.dotnet

Build the image:  
`docker build . -f SonarDotnet.Dockerfile -t gmarokov/sonar.dotnet:5.0`