# Image with Dotnet SDK, Java runtime and SonarCloud MSBuild dotnet tool

FROM mcr.microsoft.com/dotnet/sdk:5.0-focal
ENV PATH="$PATH:/root/.dotnet/tools"

# Install Java Runtime
RUN apt-get update
RUN apt install default-jre -y

# Install SonarCloud dotnet tool
RUN dotnet tool install --global dotnet-sonarscanner

# Install Trx2Sonar converter dotnet tool
RUN dotnet tool install --global dotnet-trx2sonar