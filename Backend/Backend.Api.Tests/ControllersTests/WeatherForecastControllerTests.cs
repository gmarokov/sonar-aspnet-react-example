using Backend.Api.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Backend.Api.Tests.ControllersTests
{
    public class WeatherForecastControllerTests
    {
        [Fact]
        public void Get_Returns_ListOf_WeatherForecast()
        {
            //Arrange
            var controller = new WeatherForecastController(Mock.Of<ILogger<WeatherForecastController>>());

            //Act
            var result = controller.Get();

            //Assert
            Assert.NotEmpty(result);
        }
    }
}
